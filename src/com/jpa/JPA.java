/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.jpa;

import com.entidades.Producto;
import com.entidades.*;
import com.controlador.ProductoControlador;
import java.math.BigDecimal;


/**
 *
 * @author DELL
 */
public class JPA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        double val = 12.49;
        BigDecimal bd = BigDecimal.valueOf(val);
        ProductoControlador pc = new ProductoControlador();
        Categoria ca = new Categoria(1,"nom","des");
        Marca ma = new Marca(1,"nomMa","desMa");
        Proveedor pr = new Proveedor(1,"nomp","desp","telp");
        Producto p = new Producto(null,"Mouse HP", bd, ca, ma, pr);
        pc.Crear(p);
    }
    
}
