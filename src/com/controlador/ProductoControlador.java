/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.controlador;

import com.conexion.Conexion;
import com.entidades.Producto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.*;

/**
 *
 * @author Kevin Uribe, Jong Yang, Gerson González, Rogelio Mejía
 */
public class ProductoControlador {

    private EntityManager entityManager(){
        return Conexion.getInstancia().getFabrica().createEntityManager();
    }
    
    public void Crear (Producto p) {
        EntityManager em = entityManager();
        try {
            em.getTransaction().begin();
            em.persist(p); //sirve para agregar el registro
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            JOptionPane.showMessageDialog(null, "Error al agregar registro: "+e,"Advertencia",1);
        }
    }
    
    public void Editar (Producto p) {
        EntityManager em = entityManager();
        try {
            em.getTransaction().begin();
            em.merge(p); //sirve para actualizar el registro
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            JOptionPane.showMessageDialog(null, "Error al actualizar registro: "+e,"Advertencia",1);
        }
    }
    
    public void Eliminar (Producto p) {
        EntityManager em = entityManager();
        try {
            em.getTransaction().begin(); //inicia la transacción
            em.remove(p); //sirve para quitar el registro
            em.getTransaction().commit(); //inserta la transacción
        } catch (Exception e) {
            em.getTransaction().rollback();
            JOptionPane.showMessageDialog(null, "Error al actualizar registro: "+e,"Advertencia",1);
        }
    }
    
    public List<Producto> Leer(){
        Query q = entityManager().createQuery("SELECT p FROM producto p");
        return q.getResultList();//agregar mayuscula Producto si no funciona
    }
    
}
