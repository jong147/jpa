/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.conexion;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Kevin Uribe, Jong Yang, Gerson González, Rogelio Mejía
 */
public class Conexion {
    
    //singleton
    private static Conexion instancia = new Conexion();
    private EntityManagerFactory fabrica;
    
    private Conexion() {
    fabrica = Persistence.createEntityManagerFactory("JPAPU");
    }

    public static Conexion getInstancia() {
        return instancia;
    }

    public EntityManagerFactory getFabrica() {
        return fabrica;
    }
    
}
